import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { HomeComponent } from "./home/home.component";
import { AdvancedSearchComponent } from "./advanced-search/advanced-search.component";
import { CreateDeckComponent } from "./create-deck/create-deck.component";
import { CollectionComponent } from "./collection/collection.component";

@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrl: './app.component.scss',
    imports: [RouterOutlet, NavBarComponent, HomeComponent, AdvancedSearchComponent, CreateDeckComponent, CollectionComponent]
})
export class AppComponent {
  title = 'Digimon-App';
}
