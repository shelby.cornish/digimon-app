interface DigimonCard {
  name: string;
  digiPower: number;
  ability: string;
  attribute: string;
  color: string;
  memoryCost: number;
  level: number;
  inheritedAbility: string;
  digiVolveCost: number;
  cardNumber: {
    number: number;
    setID: string;
  };
  rarity: string;
  alternativeArt: boolean;
  stage: string;
  securityEffect: string;
  type: string;
}

interface OptionCard {
  name: string;
  memoryCost: number;
  ability: string;
  securityEffect: string;
  color: string;
  cardNumber: {
    number: number;
    setID: string;
  };
  rarity: string;
  alternativeArt: boolean;
}

interface TamerCard {
  name: string;
  memoryCost: number;
  ability: string;
  securityEffect: string;
  color: string;
  type: string;
  cardNumber: {
    number: number;
    setID: string;
  };
  rarity: string;
  inheritedAbility: string;
  alternativeArt: boolean;
}

interface digiEgg {
  name: string;
  memoryCost: number;
  color: string;
  type: string;
  attribute: string;
  cardNumber: {
    number: number;
    setID: string;
  };
  rarity: string;
  inheritedAbility: string;
  alternativeArt: boolean;
}
